package com.classofjava.dao.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.classofjava.dao.UserDao;
import com.classofjava.model.User;
import com.classofjava.model.impl.JpaDepartment;
import com.classofjava.model.impl.JpaUser;

@ContextConfiguration(locations = "classpath:test-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDaoTest {

	@Autowired
	UserDao userDao;

	@Test
	public void testSaveUser() {
		JpaUser user = new JpaUser();
		user.setName("Hello");
		user.setUserPassword("11");
		userDao.save(user);
	}


}

package com.classofjava.dao.impl;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.classofjava.dao.CompanyDao;
import com.classofjava.model.Company;
import com.classofjava.model.impl.JpaCompany;
import com.classofjava.model.impl.JpaDepartment;

@ContextConfiguration(locations = "classpath:test-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class CompanyDaoTest {

	@Autowired
	CompanyDao companyDao;

	@Test
	@Ignore
	public void testSaveCompanyAndDepartment() {
		JpaCompany company = new JpaCompany(null, "Inteleos Inc",
				"Pharma Company");

		JpaDepartment department1 = new JpaDepartment(null, "HR",
				"Human Resources", "Human Resources Department");

		JpaDepartment department2 = new JpaDepartment(null, "OPR", "Operations",
				"Operations Department");

		department1.setCompany(company);
		company.getDepartments().add(department1);

		department2.setCompany(company);
		company.getDepartments().add(department2);

		companyDao.save(company);
	}
	
	@Test
	public void testFindCompany() {
		Company company = companyDao.find("Inteleos Inc");
		Assert.assertNotNull(company);
	}

}

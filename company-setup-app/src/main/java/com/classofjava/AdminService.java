package com.classofjava;

import java.util.Set;

public interface AdminService {

	public void createCompany(Company company);

	public void createDepartment(Department newDepartment, Company toCompany);

	public void createRole(Role role);

	public void createUser(User user, Role role);

	public void addUser(User user, Department toDepartment);

	public void enable(User user);

	public boolean isEnabled(User user);

	public void disable(User user);

	public Set<Company> getCompanies();

	public Set<Role> getRoles();

	public boolean isUserInRole(User user, Role inRole);

}

package com.classofjava;

public interface Role {

	int getId();

	String getName();

}

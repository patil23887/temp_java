package com.classofjava.support;

import com.classofjava.Department;

public final class DefaultDepartment implements Department {

	private long id;

	private String name;

	public DefaultDepartment(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultDepartment other = (DefaultDepartment) obj;
		if (id != other.id)
			return false;
		return true;
	}

}

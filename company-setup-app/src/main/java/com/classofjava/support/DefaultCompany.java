package com.classofjava.support;

import java.util.Set;

import com.classofjava.Company;
import com.classofjava.Department;

public final class DefaultCompany implements Company {

	private long id;

	private String name;

	private Set<Department> departments;

	public DefaultCompany(long id, String name, Set<Department> departments) {
		super();
		this.id = id;
		this.name = name;
		this.departments = departments;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Set<Department> getDepartments() {
		return departments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultCompany other = (DefaultCompany) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}

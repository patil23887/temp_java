package com.classofjava.support;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.classofjava.AdminService;
import com.classofjava.Company;
import com.classofjava.Department;
import com.classofjava.Role;
import com.classofjava.User;
import com.classofjava.dao.CompanyDao;
import com.classofjava.dao.DepartmentDao;
import com.classofjava.dao.RoleDao;
import com.classofjava.dao.UserDao;
import com.classofjava.model.impl.JpaCompany;

@Service
public final class DefaultAdminService implements AdminService {

	private static final Logger log = LoggerFactory
			.getLogger(DefaultAdminService.class);

	
	private CompanyDao companyDao;

	
	private DepartmentDao departmentDao;

	
	private RoleDao roleDao;

	
	private UserDao userDao;

	@Override
	public void createCompany(Company company) {
		if (company == null || company.getId() < 0 || company.getName() == null) {
			throw new RuntimeException();
		}
		boolean exist = companyDao.exist(company.getName());
		if (exist) {
			log.error("Trying to create company, which already exists");
			throw new RuntimeException(String.format(
					"Company Already Exists!! name : %s", company.getName()));
		}

		if (log.isDebugEnabled()) {
			log.debug(String.format("Creating company with name : %s",
					company.getName()));
		}
		companyDao
				.save(new JpaCompany(company.getId(), company.getName(), null));
	}

	@Override
	public void createDepartment(Department newDepartment, Company toCompany) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createRole(Role role) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createUser(User user, Role role) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addUser(User user, Department toDepartment) {
		// TODO Auto-generated method stub

	}

	@Override
	public void enable(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isEnabled(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void disable(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<Company> getCompanies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Role> getRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUserInRole(User user, Role inRole) {
		// TODO Auto-generated method stub
		return false;
	}

}

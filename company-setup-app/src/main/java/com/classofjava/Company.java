package com.classofjava;

import java.util.Set;

public interface Company {

	public long getId();

	public String getName();
	
	Set<Department> getDepartments();

}

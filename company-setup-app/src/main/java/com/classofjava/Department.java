package com.classofjava;

public interface Department {

	public long getId();

	public String getName();

}

package com.classofjava.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.classofjava.model.Activity;

@Entity
@Table(name = "Activity")
public class JpaActivity implements Activity{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")	
	private long id;
	
	@Column(name = "Name")
	private String Name;
	
	@Override
	public long getId() {
		return id;
	}
	@Override
	public void setId(long id) {
		this.id = id;
	}
	@Override
	public String getName() {
		return Name;
	}
	@Override
	public void setName(String name) {
		Name = name;
	}
	
}

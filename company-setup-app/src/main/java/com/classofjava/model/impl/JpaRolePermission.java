package com.classofjava.model.impl;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.classofjava.model.Permission;
import com.classofjava.model.Role;
import com.classofjava.model.RolePermission;
import com.classofjava.model.RolePermissionPK;


@Entity
@Table(name = "role_permission")
public class JpaRolePermission implements RolePermission {
	
	@EmbeddedId
	private JpaRolePermissionPK id;

	@ManyToOne(targetEntity = JpaRole.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "RoleId", insertable = false, updatable = false)
	private Role role;
	
	@ManyToOne(targetEntity = JpaPermission.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "PermissionId", insertable = false, updatable = false)
	private Permission permission;

	@Column(name = "createdBy")
	private long createdBy;
	
	public RolePermissionPK getId() {
		return id;
	}
	public void setId(RolePermissionPK id) {
		this.id = (JpaRolePermissionPK) id;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Permission getPermission() {
		return permission;
	}
	public void setPermission(Permission permission) {
		this.permission = permission;
	}
	public long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	
	
}

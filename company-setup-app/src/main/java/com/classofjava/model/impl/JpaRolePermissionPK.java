package com.classofjava.model.impl;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.classofjava.model.RolePermissionPK;

@Embeddable
public class JpaRolePermissionPK implements RolePermissionPK, Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer roleId;

	private Integer permissionId;
	
	public JpaRolePermissionPK(){
		
	}

	@Override
	public Integer getRoleId() {
		return roleId;
	}

	@Override
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Override
	public Integer getPermissionId() {
		return permissionId;
	}

	@Override
	public void setPermissionId(Integer permissionId) {
		this.permissionId = permissionId;
	}

	
	

}

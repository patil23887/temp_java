package com.classofjava.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.classofjava.model.Department;
import com.classofjava.model.Role;
import com.classofjava.model.User;

@Entity
@Table(name = "User")
@NamedQuery(name = "getUserAndDepartment", query = "select u from JpaUser u inner join fetch u.department where u.id =:id")
@NamedQueries(value = { @NamedQuery(name = "usernameLike", query = "select u from JpaUser where u.name like =:%name") })
public class JpaUser implements User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private Long id;

	@Column(name = "Name")
	private String name;

	@Column(name = "userPassword", length = 5)
	private String userPassword;

	@ManyToOne(targetEntity = JpaRole.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "RoleId", insertable = false, updatable = false)
	private Role role;

	@ManyToOne(targetEntity = JpaDepartment.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "DepartmentId", insertable = false, updatable = false)
	private Department department;

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getuserPassword() {
		// TODO Auto-generated method stub
		return null;
	}

}

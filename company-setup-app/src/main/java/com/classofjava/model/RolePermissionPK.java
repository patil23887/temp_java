package com.classofjava.model;

public interface RolePermissionPK {
	
	public Integer getRoleId() ;

	public void setRoleId(Integer roleId);

	public Integer getPermissionId() ;

	public void setPermissionId(Integer permissionId) ;

	
	


}

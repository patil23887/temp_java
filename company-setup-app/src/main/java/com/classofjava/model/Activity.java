package com.classofjava.model;

public interface Activity {
	
	public long getId() ;
	public void setId(long id) ;
	public String getName() ;
	public void setName(String name) ;
	

}

package com.classofjava.model;

public interface User {
	Long getId();

	String getName();

	String getuserPassword();
}

package com.classofjava;

public interface User {

	int getId();

	String getName();

	Role getRole();
}

package com.classofjava.dao;

import com.classofjava.model.Company;

public interface CompanyDao {

	public void save(Company company);

	public void update(Company company);

	boolean exist(String companyName);

	Company find(String companyName);

	Company find(long companyId);
}

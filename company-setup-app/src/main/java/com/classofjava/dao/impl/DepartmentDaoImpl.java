package com.classofjava.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.classofjava.dao.DepartmentDao;
import com.classofjava.model.Department;
import com.classofjava.model.impl.JpaDepartment;

@Repository
public class DepartmentDaoImpl implements DepartmentDao {

	@PersistenceContext(unitName = "companySetup")
	EntityManager entityManager;

	@Override
	@Transactional(value = "transactionManager", readOnly = false)
	public void save(Department department) {
		entityManager.persist(department);
	}

	@Override
	@Transactional(value = "transactionManager", readOnly = false)
	public Department update(Department department) {
		return entityManager.merge(department);
	}

	@Override
	public Department find(Long departmentId) {
		return entityManager.find(JpaDepartment.class, departmentId);
	}

}

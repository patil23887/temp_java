package com.classofjava.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.classofjava.dao.UserDao;
import com.classofjava.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	@PersistenceContext(unitName = "companySetup")
	EntityManager entityManager;

	@Override
	@Transactional(value = "transactionManager", readOnly = false)
	public void save(User user) {
		entityManager.persist(user);
	}

	public User findUserById(Long id) {
		TypedQuery<User> userAndDepartment = entityManager.createNamedQuery(
				"getUserAndDepartment", User.class).setParameter("id", id);
		User user = userAndDepartment.getSingleResult();
		return user;
	}

	public List<User> findUsers(String nameLike) {
		TypedQuery<User> userAndDepartment = entityManager.createNamedQuery(
				"getUserAndDepartment", User.class).setParameter("nameLike",
				nameLike);
		List<User> resultList = userAndDepartment.getResultList();
		return resultList;
	}

}

package com.classofjava.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.classofjava.dao.CompanyDao;
import com.classofjava.dao.DepartmentDao;
import com.classofjava.model.Company;
import com.classofjava.model.Department;
import com.classofjava.model.impl.JpaCompany;

@Repository
public class CompanyDaoImpl implements CompanyDao {

	@PersistenceContext(unitName = "companySetup")
	private EntityManager entityManager;

	@Autowired
	DepartmentDao departmentDao;

	@Override
	@Transactional(value = "transactionManager", readOnly = false)
	public void save(Company company) {
		entityManager.persist(company);
		for (Department department : company.getDepartments()) {
			entityManager.persist(department);
		}
	}

	@Override
	public void update(Company company) {
		entityManager.merge(company);
	}

	@Override
	public Company find(long companyId) {
		if (companyId == 0L) {
			return null;
		}
		return entityManager.find(JpaCompany.class, Long.valueOf(companyId));
	}

	@Override
	public Company find(String companyName) {
		TypedQuery<JpaCompany> q = entityManager.createNamedQuery(
				"findCompanyByName", JpaCompany.class).setParameter("name", companyName);
		Company company = q.getSingleResult();
		return company;
	}

	@Override
	public boolean exist(String companyName) {
		Company company = find(companyName);
		return company != null;
	}

}

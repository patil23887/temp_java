package com.classofjava.dao;

import com.classofjava.model.Department;

public interface DepartmentDao {

	public void save(Department department);

	public Department update(Department department);

	public Department find(Long departmentId);

}
